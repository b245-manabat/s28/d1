// CRUD Operations
	// CRUD Operation is the heart of any backend application
	// mastering the CRUD operations is essential for any developer
	// This helps in building character and increasing exposure to logical statements that will help us manipulate
	// mastering the CRUD operations of any languages makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.

// [SECTION] Create (Inserting document)
	// since MongoDB deals with object as it's structure for documents, we can easily create them by providing objects into our methods.
	// mongoDB shell also uses javascript for it's syntax which makes it convenient for us to understand it's code.

	// Insert One document
		/*
			Syntax:
				db.collectionName.insertOne({object/document})
		*/

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "123456789",
			email: "janedoe@gmail.com",
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	});

	// Insert Many
		/*
			db.user.insertMany([
			{objectA},
			{objectB},
			. . .
			])
		*/

	db.users.insertMany([
			{
				firstName: "Stephen",
				lastName: "Hawking",
				age: 76,
				contact: {
					phone: "98127352",
					email: "stephenhawkings@gmail.com"
				},
				courses: ["Python", "React", "PHP"],
				department: "none"
			},
			{
				firstName: "Niel",
				lastName: "Armstrong",
				age: 82,
				contact: {
					phone: "1256132421",
					email: "nielarmstrong@gmail.com"
				},
				courses: ["React", "Laravel", "SASS"],
				department: "none"
			}
		]);

// [SECTION] Find (read)
	/*
		Syntax:
			// it will show us all the documents in our collection
			db.collectionName.find();
	
			// it will show us all the documents that has the given fieldset
			db.collectionName.find({field: value});
	*/

	db.users.find();

	db.users.find({age : 76});

	db.users.find({firstName : "Jane"});

		// finding odcuments using multiple fieldsets
	/*
		Syntax:
			db.collectionName.find({fieldA:valueA,fieldB:valueB})
	*/

	db.users.find({lastName:"Armstrong", age:82});

// [SECTION] Updating Documents

	// add document

	db.users.insertOne({
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contact: {
			phone: "0000000",
			email: "test@gmail.com"
		},
		courses: [],
		department: "none"
	})

	// updateOne
		/*
			Syntax:
				db.collectionName.updateOne({criteria}, {$set: {field:value}});
		*/

	db.users.updateOne({
		firstName: "Jane"}, {
			$set: {
				lastName: "Hawking";
			}
	});

		// updating multiple documents
		/*
			db.collectionName.updateMany({criteria},{
				$set: {
					field:value
				}
			})
		*/

	db.users.updateMany({firstName: "Jane"},{
		$set: {
			lastName: "Wick",
			age:25
		}
	});

	db.users.updateMany({department: "none"},{
		$set: {
			department: "HR"
		}
	});

		// replacing the old document
		/*
			db.users.replaceOne({criteria}, {document/objectToReplace})
		*/

	db.users.replaceOne({firstName:"Test"}, {
		firstName: "Loven",
		lastName: "Manabat"
	});

// [SECTION] Deleting documents
	// Deleting single document
		/*
			db.collectionName.deleteOne({criteria});
		*/

	db.users.deleteOne({firstName:"Jane"});

	// delete many
		/*
			db.collectionName.deleteMany({criteria});
		*/

	db.users.deleteMany({firstName:"Jane"});

		// reminder
			// db.collectionName.deleteMany({});
			// remind: dont forget to add criteria

// [SECTION] Advance Queries
	// embedded - object
	// nested - array

	// query on an embedded document
	db.users.find({"contact.phone":"123456789"});

	// query on a nested document
	db.users.find({courses: ["CSS","JavaScript","Python"]});

	// querying an array without regard to order an elements
	db.users.find({courses: {$all:["React"]}});